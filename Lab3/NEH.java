package LAB3;

import java.util.*;
import java.io.*;
import java.util.concurrent.*;

// Interfejs nasłuchujący dla obliczonego Cmax
interface CmaxListener {
    void onCmaxCalculated(int cmax);
}

// Klasa Quest reprezentująca zadanie, implementuje interfejs Comparable
class Quest implements Comparable<Quest> {
    public int sum_of_times; // Suma czasów wykonania zadania
    public ArrayList<Integer> time_exec = new ArrayList<>(); // Lista czasów wykonania poszczególnych operacji
    public ArrayList<Integer> start_quest = new ArrayList<>(); // Lista czasów rozpoczęcia kolejnych operacji w zadaniu
    private int Cmax; // Cmax dla zadania

    // Metoda obliczająca sumę czasów wykonania zadania
    public void count_sum() {
        sum_of_times = 0;
        for (Integer integer : time_exec) {
            sum_of_times += integer;
        }
    }

    // Metoda ustawiająca wartość Cmax dla zadania
    public void setCmax(int cmax) {
        this.Cmax = cmax;
    }

    // Metoda zwracająca wartość Cmax dla zadania
    public int getCmax() {
        return Cmax;
    }

    // Metoda porównująca zadania na podstawie sumy czasów wykonania
    @Override
    public int compareTo(Quest q) {
        return Integer.compare(q.sum_of_times, this.sum_of_times);
    }
}

// Główna klasa NEH
public class NEH {
    private final ArrayList<Quest> quest1 = new ArrayList<>(); // Lista zadań
    private int n, m; // Liczba zadań i liczba maszyn
    private int Cmax; // Wartość Cmax
    private int lineNumber = 0; // Numer linii w pliku

    // Metoda wczytująca dane z pliku
    public boolean loadFile() {
        try {
            File file = new File("C:/Users/neosh/IdeaProjects/Lab1/src/LAB3/NEH.DAT");
            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine().trim();
                if (line.isEmpty() || line.startsWith("data.")) {
                    continue;
                }
                if (line.equals("neh:")) {
                    loadNEH(scanner);
                } else {
                    String[] parts = line.split("\\s+");
                    Quest tmp = new Quest();
                    for (String part : parts) {
                        int time = Integer.parseInt(part);
                        tmp.time_exec.add(time);
                    }
                    tmp.count_sum();
                    quest1.add(tmp);
                }
            }
            scanner.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    // Metoda wczytująca dane specyficzne dla NEH
    public void loadNEH(Scanner scanner) {
        int nehData = scanner.nextInt();
        if (lineNumber < 99 && lineNumber != 0) {
            System.out.println("NEH data.0" + lineNumber++ + ":" + nehData); // Wyświetlanie danych NEH z numerem linii
        } else {
            System.out.println("NEH data." + lineNumber++ + ":" + nehData); // Wyświetlanie danych NEH z numerem linii
        }
    }

    // Metoda aktualizująca wartość Cmax
    private synchronized void updateCmax(int cmax, int[] tmp) {
        if (cmax < tmp[0]) {
            tmp[1] = tmp[0];
            tmp[0] = cmax;
        } else if (cmax < tmp[1]) {
            tmp[1] = cmax;
        }
    }

    // Wewnętrzna klasa reprezentująca zadanie obliczania Cmax w osobnym wątku
    private class CalculateCmaxTask implements Runnable {
        private Quest quest;
        private int[] tmp;
        private final CmaxListener listener;

        // Konstruktor klasy
        public CalculateCmaxTask(Quest quest, int[] tmp, CmaxListener listener) {
            this.quest = quest;
            this.tmp = tmp;
            this.listener = listener;
        }

        // Metoda wykonująca obliczenia Cmax dla zadania
        @Override
        public void run() {
            int cmax = calculateCmaxForQuest();
            updateCmax(cmax, tmp);
            listener.onCmaxCalculated(cmax);
        }

        // Metoda obliczająca Cmax dla danego zadania
        private int calculateCmaxForQuest() {
            quest.start_quest.add(0);
            for (int i = 0; i < m - 1; i++) {
                quest.start_quest.add(quest.start_quest.get(i) + quest.time_exec.get(i));
            }
            for (int i = 1; i < quest1.size(); i++) {
                quest.start_quest.add(quest1.get(i - 1).start_quest.get(0) + quest1.get(i - 1).time_exec.get(0));
            }
            for (int j = 1; j < quest1.size(); j++) {
                for (int i = 1; i < m; i++) {
                    quest.start_quest.add(Math.max(quest1.get(j - 1).time_exec.get(i) + quest1.get(j - 1).start_quest.get(i), quest1.get(j).time_exec.get(i - 1) + quest1.get(j).start_quest.get(i - 1)));
                }
            }
            return (quest.start_quest.get(quest.start_quest.size() - 1) + quest.time_exec.get(quest.time_exec.size() - 1));
        }
    }

    // Metoda implementująca algorytm NEH
    public void algorithmNEH() {
        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        for (int i = 0; i < n; i++) {
            Quest quest = quest1.get(i);
            Runnable task = new CalculateCmaxTask(quest, new int[2], new CmaxListener() {
                @Override
                public void onCmaxCalculated(int cmax) {
                    quest.setCmax(cmax);
                }
            });
            executor.execute(task);
        }

        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Metoda główna programu
    public static void main(String[] args) {
        double startTime = System.currentTimeMillis();
        NEH ob = new NEH();

        if (ob.loadFile()) {
            ob.algorithmNEH();
            double endTime = System.currentTimeMillis();
            double executionTime = endTime - startTime;
            System.out.println("Czas wykonania programu: " + executionTime / 1000 + " sekundy");
            System.out.println("\nZakończenie wykonania programu.");
        }
    }
}
