package LAB1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SchrageAlgorithm {

    static class Task implements Comparable<Task> {
        int index; // Numer porządkowy wiersza w pliku
        int r; // Czas pojawienia się zadania
        int p; // Czas wykonania zadania
        int q; // Czas dostarczenia zadania

        Task(int index, int r, int p, int q) {
            this.index = index;
            this.r = r;
            this.p = p;
            this.q = q;
        }
        public int compareTo(Task other) {
            return Integer.compare(this.r, other.r);
        }
    }
    public static void main(String[] args) {
        double totalExecutionTime = 0;
        String[] fileNames = {"data1.dat", "data2.dat", "data3.dat", "data4.dat"};
        String directoryPath = "C:/Users/neosh/IdeaProjects/Lab1/src/LAB1/";
        int totalMaxQ = 0;

        for (String fileName : fileNames) {
            ArrayList<Task> tasks = new ArrayList<>();

            try {
                Scanner scanner = new Scanner(new File(directoryPath + fileName));
                int n = scanner.nextInt();
                // Odczytujemy zadania z pliku, dodajemy numer porządkowy wiersza i dodajemy do listy
                for (int i = 1; i <= n; i++) {
                    int r = scanner.nextInt();
                    int p = scanner.nextInt();
                    int q = scanner.nextInt();
                    tasks.add(new Task(i, r, p, q));
                }
                scanner.close();

                // Sortujemy zadania według czasu pojawienia się (r)
                Collections.sort(tasks);

                // Wyświetlamy numer porządkowy wiersza w pliku dla każdego zadania w kolejności wykonywania
                for (Task task : tasks) {
                    System.out.print(task.index + " ");
                }
                System.out.println();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                continue;
            }

            long startTime = System.currentTimeMillis(); // Początek pomiaru czasu wykonania algorytmu

            int currentTime = 0; // Aktualny czas
            int maxQ = 0; // Czas zakończenia ostatniej operacji dla aktualnego pliku
            ArrayList<Task> schedule = new ArrayList<>(); // Harmonogram zadań do wykonania
            ArrayList<Task> tasksCopy = new ArrayList<>(tasks); // Kopia listy zadań dla aktualnego pliku

            // Główna pętla algorytmu Schrage
            while (!tasksCopy.isEmpty() || !schedule.isEmpty()) {
                // Wybieramy zadania, które mogą być wykonane w aktualnym czasie
                int finalCurrentTime = currentTime;
                while (!tasksCopy.isEmpty() && tasksCopy.stream().anyMatch(task -> task.r <= finalCurrentTime)) {
                    int finalCurrentTime1 = currentTime;
                    // Wybieramy zadanie o największym czasie dostarczenia z tych, które mogą być już wykonywane
                    Task eligibleTask = tasksCopy.stream()
                            .filter(task -> task.r <= finalCurrentTime1)
                            .max((t1, t2) -> Integer.compare(t1.q, t2.q))
                            .orElse(Collections.min(tasksCopy)); // Jeśli nie ma pasującego zadania, wybieramy to o najmniejszym czasie pojawienia się
                    tasksCopy.remove(eligibleTask); // Usuwamy zadanie z listy zadań do wykonania
                    schedule.add(eligibleTask); // Dodajemy zadanie do harmonogramu
                    currentTime = Math.max(currentTime, eligibleTask.r); // Aktualizujemy currentTime
                }

                // Jeśli harmonogram jest pusty, aktualizujemy currentTime do czasu pojawienia się najwcześniejszego zadania
                if (schedule.isEmpty()) {
                    currentTime = tasksCopy.stream().map(task -> task.r).min(Integer::compareTo).orElse(currentTime);
                } else {
                    // Wybieramy najwcześniejsze zadanie z harmonogramu
                    Task currentTask = schedule.remove(0);
                    currentTime += currentTask.p; // Aktualizujemy currentTime o czas wykonania aktualnego zadania
                    maxQ = Math.max(maxQ, currentTime + currentTask.q); // Aktualizujemy czas zakończenia ostatniej operacji
                }
            }

            long endTime = System.currentTimeMillis(); // Koniec pomiaru czasu wykonania algorytmu
            long executionTime = endTime - startTime; // Obliczenie czasu wykonania dla aktualnego pliku
            totalMaxQ += maxQ; // Dodajemy wynik bieżącego pliku do łącznej sumy
            totalExecutionTime += executionTime; // Dodajemy czas wykonania bieżącego pliku do łącznej sumy czasu wykonania

            System.out.println("Długość uszeregowań " + fileName + ": " + maxQ);
            System.out.println("Czas wykonania algorytmu dla pliku " + fileName + ": " + executionTime + " ms");
        }

        System.out.println("Całkowity czas zakończenia ostatniej operacji dla wszystkich plików: " + totalMaxQ);
        System.out.println("Całkowity czas wykonania algorytmu Schrage dla wszystkich plików: " + totalExecutionTime / 1000 + " s");
    }
}