package LAB5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CarlierAlgorithm {
    public static int schrage(int n, int[] R, int[] P, int[] Q, int[] X) {
        int[] ND = new int[100]; // Wykorzystuje tablice ND (indeksy zadań do przetworzenia) i D (zadania gotowe do realizacji) do przechowywania informacji o zadaniach.
        int[] D = new int[100]; // Tablica zadań gotowych do realizacji
        int nd = n, d = 0, w = 0, t = 0, cmax = 0; // Liczniki i czas
        for (int i = 0; i < n; i++) {
            ND[i] = i; // Inicjalizacja tablicy indeksów
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                if (R[ND[j]] < R[ND[j + 1]]) { // Sortowanie indeksów po R
                    int temp = ND[j];
                    ND[j] = ND[j + 1];
                    ND[j + 1] = temp;
                }
            }
        }
        while (w != n) { // Dopóki nie przetworzymy wszystkich zadań
            if (nd != 0) { // Jeśli są zadania do przetworzenia
                if (R[ND[nd - 1]] <= t) { // Jeśli zadanie może być zrealizowane
                    D[d] = ND[nd - 1]; // Dodajemy je do tablicy gotowych zadań
                    d++;
                    nd--;
                    for (int k = d - 1; k > 0; k--) { // Sortowanie zadań gotowych po Q
                        if (Q[D[k]] < Q[D[k - 1]]) {
                            int temp = D[k];
                            D[k] = D[k - 1];
                            D[k - 1] = temp;
                        }
                    }
                    continue;
                }
            }
            if (d != 0) { // Jeśli są zadania gotowe do realizacji
                X[w] = D[d - 1]; // Realizujemy zadanie o najwyższym Q
                t += P[X[w]];
                cmax = Math.max(cmax, t + Q[X[w]]);
                d--;
                w++;
                continue;
            }
            if (d == 0 && R[ND[nd - 1]] > t) { // Jeśli brak zadań gotowych, ale są jeszcze zadania do przetworzenia
                t = R[ND[nd - 1]]; // Przewijamy czas do momentu pojawienia się zadania
            }
        }
        return cmax; // Zwracamy wartość cmax
    }

    public static int schrage_podziel(int n, int[] R, int[] P, int[] Q) {
        int[] ND = new int[100]; // Tablica indeksów zadań do przetworzenia
        int[] D = new int[100]; // Tablica zadań gotowych do realizacji
        int[] pom = new int[100]; // Pomocnicza tablica czasów wykonania zadań
        int nd = n, d = 0, w = 0, t = 0, cmax = 0, poz = 100, ile_zr = 0; // Liczniki, czas, pozycja, ilość zrealizowanych
        for (int i = 0; i < n; i++) {
            ND[i] = i; // Inicjalizacja tablic do przechowywania informacji o zadaniach.
            pom[i] = P[i]; // Skopiowanie czasów wykonania
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                if (R[ND[j]] < R[ND[j + 1]]) { // Sortowanie indeksów po R
                    int temp = ND[j];
                    ND[j] = ND[j + 1];
                    ND[j + 1] = temp;
                }
            }
        }
        while (nd != 0 || d != 0) { // Dopóki są zadania do przetworzenia lub realizowane
            if (nd != 0) { // Jeśli są zadania do przetworzenia
                if (R[ND[nd - 1]] <= t) { // Jeśli zadanie może być zrealizowane
                    D[d] = ND[nd - 1]; // Dodajemy je do tablicy gotowych zadań
                    d++;
                    nd--;
                    for (int k = d - 1; k > 0; k--) { // Sortowanie zadań gotowych po Q
                        if (Q[D[k]] < Q[D[k - 1]]) {
                            int temp = D[k];
                            D[k] = D[k - 1];
                            D[k - 1] = temp;
                        }
                    }
                    if (poz != 100) { // Jeśli jest jakaś zapamiętana pozycja
                        if (Q[D[d - 1]] > Q[poz]) { // Jeśli Q zapamiętanego zadania jest większe od aktualnego
                            D[d] = poz; // Wstawiamy zapamiętane zadanie na koniec tablicy gotowych zadań
                            int temp = D[d];
                            D[d] = D[d - 1];
                            D[d - 1] = temp;
                            d++;
                            poz = 100; // Resetujemy pozycję
                        }
                    }
                    continue;
                }
            }
            if (d != 0) { // Jeśli są zadania gotowe do realizacji
                if (poz == 100) { // Jeśli nie ma zapamiętanej pozycji
                    poz = D[d - 1]; // Zapamiętujemy ostatnie zadanie
                    d--;
                }
                if (nd != 0) { // Jeśli są jeszcze zadania do przetworzenia
                    ile_zr = Math.min(pom[poz], R[ND[nd - 1]] - t); // Ile można zrealizować
                } else {
                    ile_zr = pom[poz]; // Jeśli nie ma więcej zadań, realizujemy całe zapamiętane
                }
                t += ile_zr; // Aktualizacja czasu
                pom[poz] -= ile_zr; // Zmniejszenie czasu realizacji zadania
                if (pom[poz] == 0) { // Jeśli zadanie zostało zrealizowane
                    cmax = Math.max(cmax, t + Q[poz]); // Aktualizacja cmax
                    poz = 100; // Reset pozycji
                }
                continue;
            }
            if (d == 0 && nd != 0) { // Jeśli brak zadań gotowych, ale są jeszcze zadania do przetworzenia
                if (R[ND[nd - 1]] > t) { // Przewijamy czas do momentu pojawienia się zadania
                    t = R[ND[nd - 1]];
                }
            }
        }
        return cmax; // Zwracamy wartość cmax
    }
    //funkcja identyfikuje bloki zadań i zwraca informacje o najdłuższym bloku
    public static void Blok(int n, int[] R, int[] P, int[] Q, int[] X, int[] cI, int[] cR, int[] cQ) {
        int posB = -1, m = 0, cmax = 0; // Pozycja bloku, czas, cmax
        //Przechodzi przez zadania w kolejności zgodnej z rozwiązaniem, zaznaczając zadania w aktualnym bloku przy użyciu tablicy pomocniczej.
        int[] tmp = new int[100]; // Tablica pomocnicza
        for (int i = 0; i < n; i++) {
            int j = X[i];
            tmp[i] = (m >= R[j]) ? 1 : 0; // Zaznaczanie zadań w bloku
            m = Math.max(m, R[j]) + P[j]; // Aktualizacja czasu
            if (cmax < m + Q[j]) { // Aktualizacja cmax
                cmax = m + Q[j];
                posB = i; // Zapamiętanie pozycji bloku
            }
        }
        int i = posB, j = -1;
        int bQ = Q[X[posB]];
        int bR = R[X[posB]];
        int bP = P[X[posB]];
        while (tmp[i] != 0) { // Przeszukiwanie bloku
            if (Q[X[--i]] < bQ) {
                j = X[i]; // Zapamiętanie zadania z mniejszym Q
                break;
            }
            bR = Math.min(bR, R[X[i]]); // Aktualizacja R
            bP += P[X[i]]; // Aktualizacja P
        }
        //Znajduje najdłuższy blok, a następnie zwraca informacje o ostatnim zadaniu w tym bloku
        cI[0] = j; // Zapisanie indeksu zadania
        cR[0] = bR + bP; // Obliczenie R
        cQ[0] = bQ + bP; // Obliczenie Q
    }
    //Wykorzystuje rekurencję do iteracyjnego wywoływania algorytmu na podproblemach
    //z założeniem, że krótsze harmonogramy uzyskane w poprzednich iteracjach mogą ograniczyć górne ograniczenie (UB).
    public static void Carlier(int n, int[] R, int[] P, int[] Q, int[] X, int[] UB) {
        if (schrage_podziel(n, R, P, Q) >= UB[0]) { // Warunek stopu
            return;
        }
        int sCmax = schrage(n, R, P, Q, X); // Obliczenie schrage
        if (sCmax < UB[0]) { // Aktualizacja UB
            UB[0] = sCmax;
        }
        int[] j = new int[1];
        int[] jr = new int[1];
        int[] jq = new int[1];
        Blok(n, R, P, Q, X, j, jr, jq); // Wywołanie funkcji Blok
        if (j[0] < 0) { // Brak bloku
            return;
        }
        int tmpR = R[j[0]]; // Zapamiętanie R
        int tmpQ = Q[j[0]]; // Zapamiętanie Q
        R[j[0]] = jr[0]; // Aktualizacja R
        Carlier(n, R, P, Q, X, UB); // Rekurencja
        R[j[0]] = tmpR; // Przywrócenie R
        Q[j[0]] = jq[0]; // Aktualizacja Q
        Carlier(n, R, P, Q, X, UB); // Rekurencja
        Q[j[0]] = tmpQ; // Przywrócenie Q
    }

    public static void main(String[] args) throws FileNotFoundException {
        int[] R = new int[100]; // Czas przygotowania
        int[] P = new int[100]; // Czas wykonania
        int[] Q = new int[100]; // Czas dostarczenia
        int[] X = new int[100]; // Kolejność realizacji
        String s = "data.0";
        String s1;
        Scanner scanner = new Scanner(new File("C:/Users/neosh/IdeaProjects/Lab1/src/LAB5/CARLIER.DAT"));
        for (int i = 0; i < 9; i++) {
            s1 = s + i + ":";
            int n = 0;
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                if (line.startsWith(s1)) { // Wyszukiwanie danych
                    // Kontynuujemy wczytywanie danych
                    n = scanner.nextInt();
                    for (int j = 0; j < n; j++) {
                        R[j] = scanner.nextInt();
                        P[j] = scanner.nextInt();
                        Q[j] = scanner.nextInt();
                    }
                    break; // Wyjście z pętli, jeśli dane znalezione
                }
            }

            int[] UB = {schrage(n, R, P, Q, X)};
            Carlier(n, R, P, Q, X, UB);
            System.out.println("———————————————————————————————————————————");
            System.out.println(s1);
            System.out.println("Schrage: " + schrage(n, R, P, Q, X));
            System.out.println("Schrage z podzialem: " + schrage_podziel(n, R, P, Q));
            System.out.println("Carlier: " + UB[0]);
        }
        scanner.close();
    }
}
