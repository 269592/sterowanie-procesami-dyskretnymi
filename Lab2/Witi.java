package LAB2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

class Task {
    int time;       //czas trwania
    int weight;     //ważność zadania
    int deadline;   //do kiedy ma się wykonać
}

class Permutation {
    ArrayList<Integer> tasks; // Permutacja zadań
    int cost;           // Koszt wykonania tej permutacji
}

public class Witi {
    // Metoda obliczająca optymalną permutację zadań za pomocą programowania dynamicznego
    public static Permutation DP_task(int n, Task[] array) {
        int N = 1 << n;
        Permutation[] F = new Permutation[N];

        // Inicjalizacja pustego podzbioru
        F[0] = new Permutation();
        F[0].cost = 0;
        F[0].tasks = new ArrayList<>();

        // Iteracja przez wszystkie podzbiory zbioru zadań
        for (int set = 1; set < N; set++)
        {
            int c = 0; // Czas wykonania podzbioru
            for (int i = 0, b = 1; i < n; i++, b <<= 1)
            {
                if ((set & b) != 0)
                {
                    c += array[i].time;
                }
            }

            F[set] = new Permutation();
            F[set].cost = Integer.MAX_VALUE; // Ustawienie początkowego kosztu na nieskończoność
            F[set].tasks = new ArrayList<>();

            // Iteracja po zadaniach w podzbiorze
            for (int k = 0, b = 1; k < n; k++, b <<= 1) {
                if ((set & b) != 0) {
                    // całkowity koszt bieżącej permutacji
                    int cost_task = F[set - b].cost + array[k].weight * Math.max(c - array[k].deadline, 0);

                    // Aktualizacja najlepszego kosztu i permutacji
                    if (cost_task < F[set].cost) {
                        F[set].cost = cost_task;
                        F[set].tasks = new ArrayList<>(F[set - b].tasks); // Skopiowanie permutacji z poprzedniego podzbioru
                        F[set].tasks.add(k + 1); // Dodanie bieżącego zadania do permutacji
                    }
                }
            }
        }

        return F[N - 1];
    }

    public static void main(String[] args) {
        double startTime = System.currentTimeMillis(); // Rejestrowanie czasu rozpoczęcia wykonywania programu

        Task[] array = new Task[100];
        try {
            Scanner read = new Scanner(new File("C:/Users/neosh/IdeaProjects/Lab1/src/LAB2/witi.data.txt"));
            int n;
            String s;
            String[] s1 = {"data.10:", "data.11:", "data.12:", "data.13:", "data.14:", "data.15:", "data.16:", "data.17:", "data.18:", "data.19:", "data.20:"};
            for (int iter = 0; iter < 11; iter++) {
                while (!read.hasNext(s1[iter])) {
                    read.next();
                }
                s = read.next();
                System.out.print(s + " ");
                n = read.nextInt();
                for (int i = 0; i < n; i++) {
                    array[i] = new Task();
                    array[i].time = read.nextInt();
                    array[i].weight = read.nextInt();
                    array[i].deadline = read.nextInt();
                }

                // Obliczanie optymalnej permutacji dla danych zadań
                Permutation optimal = DP_task(n, array);

                System.out.println("Best solution: " + optimal.cost);
                System.out.print("Task permutation: ");
                for (int i = 0; i < optimal.tasks.size(); ++i) {
                    System.out.print(optimal.tasks.get(i) + " ");
                }
                System.out.println();
            }
            read.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        double endTime = System.currentTimeMillis(); // Rejestrowanie czasu zakończenia wykonywania programu
        double executionTime = endTime - startTime; // Różnica daje nam czas wykonania
        System.out.println("Program execution time: " + executionTime/1000 + " seconds");
        System.out.println("\nTermination of program execution.");
    }
}
