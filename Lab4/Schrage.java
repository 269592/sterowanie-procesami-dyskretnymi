package LAB4;

import java.io.*;
import java.util.*;

class SchrageWithHeap {
    static class Zadanie {
        public int r;   // czas przybycia zadania
        public int p;   // czas wykonywania zadania
        public int q;   // czas dostarczenia zadania
    }

    // Algorytm Schrage z kopcem
    static int schrageWithHeap(Zadanie[] T) {
        PriorityQueue<Zadanie> readyQueue = new PriorityQueue<>((a, b) -> b.q - a.q); // kopiec gotowych zadań, sortowanie po q malejąco
        PriorityQueue<Zadanie> tasksQueue = new PriorityQueue<>(Comparator.comparingInt(a -> a.r)); // kopiec zadań nierozpoczętych, sortowanie po r rosnąco
        Collections.addAll(tasksQueue, T);
        int t = 0; // bieżący czas
        int cmax = 0; // czas maksymalny
        Zadanie currentTask;

        while (!readyQueue.isEmpty() || !tasksQueue.isEmpty()) {
            while (!tasksQueue.isEmpty() && tasksQueue.peek().r <= t) {
                readyQueue.add(tasksQueue.poll());
            }
            if (!readyQueue.isEmpty()) {
                currentTask = readyQueue.poll();
                t += currentTask.p;
                cmax = Math.max(cmax, t + currentTask.q);
            } else {
                assert tasksQueue.peek() != null;
                t = tasksQueue.peek().r;
            }
        }
        return cmax;
    }

    // Algorytm Schrage z podziałem z kopcem
    static int schragePreemptiveWithHeap(Zadanie[] T) {
        PriorityQueue<Zadanie> readyQueue = new PriorityQueue<>((a, b) -> b.q - a.q); // kopiec gotowych zadań, sortowanie po q malejąco
        PriorityQueue<Zadanie> tasksQueue = new PriorityQueue<>(Comparator.comparingInt(a -> a.r)); // kopiec zadań nierozpoczętych, sortowanie po r rosnąco
        tasksQueue.addAll(Arrays.asList(T));
        int t = 0; // bieżący czas
        int cmax = 0; // czas maksymalny
        Zadanie currentTask = null;
        while (!readyQueue.isEmpty() || !tasksQueue.isEmpty()) {
            while (!tasksQueue.isEmpty() && tasksQueue.peek().r <= t) {
                readyQueue.add(tasksQueue.poll());
            }
            if (!readyQueue.isEmpty()) {
                if (currentTask != null) {
                    readyQueue.add(currentTask);
                }
                currentTask = readyQueue.poll();
                assert currentTask != null;
                int execTime = Math.min(currentTask.p, tasksQueue.isEmpty() ? Integer.MAX_VALUE : tasksQueue.peek().r - t);
                currentTask.p -= execTime;
                t += execTime;
                if (currentTask.p == 0) {
                    cmax = Math.max(cmax, t + currentTask.q);
                    currentTask = null;
                }
            } else {
                assert tasksQueue.peek() != null;
                t = tasksQueue.peek().r;
            }
        }
        return cmax;
    }

    // Metoda główna programu
    public static void main(String[] args) throws IOException {
        String s1;
        System.out.println("———————————————————————————————————————————");
        // Otwieranie pliku
        BufferedReader br = new BufferedReader(new FileReader("C:/Users/neosh/IdeaProjects/Lab1/src/LAB4/SCH.DAT")); // Sprawdź ścieżkę do pliku

        // Pętla przetwarzająca dane z pliku
        while (true) {
            s1 = br.readLine();

            // Sprawdzenie końca pliku
            if (s1 == null) {
                break;
            }

            // Oczekiwany format linii "data.0:"
            if (!s1.startsWith("data.")) {
                break; // Jeśli linia nie pasuje do oczekiwanego formatu, przerwij pętlę
            }

            // Pobieranie numeru zestawu danych

            // Odczyt ilości zadań
            int taskCount = Integer.parseInt(br.readLine());

            // Tworzenie tablicy zadań dla bieżącego zestawu danych
            Zadanie[] T = new Zadanie[taskCount];

            // Odczyt i inicjalizacja zadań
            for (int j = 0; j < taskCount; j++) {
                T[j] = new Zadanie();
                StringTokenizer st = new StringTokenizer(br.readLine());
                T[j].r = Integer.parseInt(st.nextToken());
                T[j].p = Integer.parseInt(st.nextToken());
                T[j].q = Integer.parseInt(st.nextToken());
            }

            // Obliczenie czasów wykonania algorytmów
            int schrageHeapTime = schrageWithHeap(T);
            int schragePreemptiveHeapTime = schragePreemptiveWithHeap(T);

            // Wyświetlanie danych bieżącego zestawu na ekranie
            System.out.println("Zbior danych: " + s1);
            System.out.println("Schrage with heap: " + schrageHeapTime);
            System.out.println("Schrage preemptive with heap: " + schragePreemptiveHeapTime);
            System.out.println("Difference: " + (schrageHeapTime - schragePreemptiveHeapTime));
            System.out.println("———————————————————————————————————————————");
        }
        // Zamykanie strumienia
        br.close();
    }
}